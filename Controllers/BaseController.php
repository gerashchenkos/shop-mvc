<?php

namespace Shop\Controllers;

use \Shop\Views\View;
use \Shop\Routers\Router;

class BaseController
{
    public $view;
    protected $exception = [];

    public function __construct()
    {
        $this->view = new View();
    }
}