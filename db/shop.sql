/*
Navicat MySQL Data Transfer

Source Server         : VagrantNew
Source Server Version : 50732
Source Host           : localhost:3306
Source Database       : test

Target Server Type    : MYSQL
Target Server Version : 50732
File Encoding         : 65001

Date: 2020-12-22 22:17:42
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cart
-- ----------------------------
DROP TABLE IF EXISTS `cart`;
CREATE TABLE `cart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of cart
-- ----------------------------
INSERT INTO `cart` VALUES ('8', '41');
INSERT INTO `cart` VALUES ('9', '41');
INSERT INTO `cart` VALUES ('10', '41');
INSERT INTO `cart` VALUES ('12', '41');
INSERT INTO `cart` VALUES ('11', '42');

-- ----------------------------
-- Table structure for cart_products
-- ----------------------------
DROP TABLE IF EXISTS `cart_products`;
CREATE TABLE `cart_products` (
  `cart_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `order_date` date DEFAULT NULL,
  PRIMARY KEY (`cart_id`,`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of cart_products
-- ----------------------------
INSERT INTO `cart_products` VALUES ('8', '32', '1', '2020-12-13');
INSERT INTO `cart_products` VALUES ('9', '31', '1', '2020-12-16');
INSERT INTO `cart_products` VALUES ('10', '31', '0', '2020-12-19');
INSERT INTO `cart_products` VALUES ('10', '32', '0', '2020-12-19');
INSERT INTO `cart_products` VALUES ('11', '31', '2', '2020-12-20');
INSERT INTO `cart_products` VALUES ('11', '32', '2', '2020-12-20');
INSERT INTO `cart_products` VALUES ('12', '31', '1', '2020-12-20');

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES ('1', 'Apple', null);
INSERT INTO `categories` VALUES ('2', 'Samsung', null);
INSERT INTO `categories` VALUES ('3', 'Xiaomi', null);

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `price` decimal(10,0) NOT NULL,
  `quantity` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES ('31', '47999', '15', '1', 'Apple iPhone 11 Pro Max', '/img/products/iphone.jpg');
INSERT INTO `products` VALUES ('32', '15799', '21', '1', 'Apple Watch Series 6 GPS', '/img/products/watch.jpg');
INSERT INTO `products` VALUES ('33', '32999', '17', '2', 'Samsung Galaxy S20', null);
INSERT INTO `products` VALUES ('34', '10999', '19', '2', 'Samsung Galaxy Watch 3', null);
INSERT INTO `products` VALUES ('35', '15999', '23', '3', 'Xiaomi Mi 10T Pro', null);
INSERT INTO `products` VALUES ('36', '1299', '14', '3', 'Xiaomi Mi Smart Band 4 NFC', null);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(40) NOT NULL,
  `type` varchar(255) DEFAULT 'user',
  `status` varchar(255) DEFAULT 'active',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'test', 't@t.com', '222', 'user', 'active');
INSERT INTO `users` VALUES ('4', 'new_test6', 't1@t.com', '1112', 'user', 'active');
INSERT INTO `users` VALUES ('5', 'new_test6', 't2@t.com', '1112', 'user', 'active');
INSERT INTO `users` VALUES ('6', 'new_test6', 't3@t.com', '1113', 'user', 'active');
INSERT INTO `users` VALUES ('7', '1', 'q@q.com', '11', 'user', 'active');
INSERT INTO `users` VALUES ('8', '2', 'q1@q.com', '11', 'user', 'active');
INSERT INTO `users` VALUES ('12', '2', 'q3@q.com', '11', 'user', 'active');
INSERT INTO `users` VALUES ('13', 'Test', 'test@test.com', 'fdd03a5bb8feb046369a2cc31413eb939d4aea54', 'user', 'not active');
INSERT INTO `users` VALUES ('25', 'Admin Test', 'admin@test.com', 'd31175f2a1f939954957aa7ccdb2422d5636d439', 'admin', 'active');
INSERT INTO `users` VALUES ('26', 'Test', '1@1.c', 'fdd03a5bb8feb046369a2cc31413eb939d4aea54', 'user', 'active');
INSERT INTO `users` VALUES ('40', 'Test', 'test12@test.com', 'fdd03a5bb8feb046369a2cc31413eb939d4aea54', 'user', 'active');
INSERT INTO `users` VALUES ('41', 'Test', 'test1@ttt.com', '1270faec21da8587d19870c9bf2acf4c66d0f408', 'user', 'active');
INSERT INTO `users` VALUES ('42', 'ser1212', 'ser@t.com', '1270faec21da8587d19870c9bf2acf4c66d0f408', 'user', 'active');
SET FOREIGN_KEY_CHECKS=1;
